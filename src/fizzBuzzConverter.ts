export type FizzBuzzType = number | string;


const fizzBuzzConverter = (number: number): FizzBuzzType => {
// 1. 让所有学生排成一队，然后按顺序报数。
// 2. 学生报数时，如果所报数字是 3 的倍数，那么不能说该数字，而要说 Fizz;如果所
//    报数字是 5 的倍数，那么要说 Buzz;如果所报数字是第 7 的倍数，那么要说
//    Whizz。
// 3. 学生报数时，如果所报数字同时是两个特殊数的倍数情况下，也要特殊处理，比如
//    3 和 5 的 倍数，那么不能说该数字，而是要说 FizzBuzz, 以此类推。如果同时是
//    三个特殊数的倍数， 那么要说 FizzBuzzWhizz。
// 4. 学生报数时，如果所报数字包含了 3，那么也不能说该数字，而是要说相应的单
//    词，比如要 报 13 的同学应该说 Fizz。
// 5. 如果数字中包含了 3，那么忽略规则 2 和规则 3，比如要报 30 的同学只报 Fizz，
//    不报 FizzBuzz 。
// 6. 如果数字中包含了 5，那么忽略规则 4 和规则 5，并且忽略被 3 整除的判定，比如
//    要报 35 的同 学不报 Fizz，报 BuzzWhizz，其他 case 自己补齐。
// 7. 如果数字中包含了 7，则忽略被 5 整除的判定，若同时还包含 5，就忽略规则 6 中
//    忽略被 3 整 除的判定，比如要报 75 的同学只报 Fizz，其他 case 自己补齐。

    if (number<1) {
       throw new RangeError("参数必须是大于等于1的数字!");  
    } else if (String(number).indexOf('7')!==-1 && String(number).indexOf('5')!==-1){
         return 'Fizz'
    }else if(String(number).indexOf('3')!==-1){
        return 'Fizz'
    } else if(number % 5 == 0 && number % 3 == 0 &&number % 7 == 0){
        return 'FizzBuzzWhizz'
    }else if(number % 15 == 0) {
        return 'FizzBuzz'
    } else if (number % 3 == 0) {
        return 'Fizz'
    } else if (number % 5 == 0){
        return 'Buzz'
    } else if(number % 7 == 0){
        return 'Whizz'
    } else {
        return String(number)
    }
}

export default fizzBuzzConverter;